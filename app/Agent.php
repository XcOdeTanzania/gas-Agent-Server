<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agent extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'email', 'phone', 'location', 'latitude', 'longitude', 'supplier_id'];
    protected $dates = ['deleted_at', 'start_date'];


    public function orders()
    {
        return $this->hasMany(Order::class);
    }
}
