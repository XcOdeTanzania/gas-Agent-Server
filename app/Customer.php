<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'email', 'phone'];
    protected $dates = ['deleted_at', 'start_date'];

    public function orders(){
        return $this->hasMany(Order::class);
    }
}
