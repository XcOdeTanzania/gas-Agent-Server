<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = ['weight', 'price', 'status', 'location', 'latitude', 'longitude', 'customer_id', 'agent_id'];
    protected $dates = ['deleted_at', 'start_date'];

    public function customers()
    {
        return $this->hasOne(Customer::class);
    }

    public function  agents(){
        return $this->hasOne(Agent::class);
    }
}
