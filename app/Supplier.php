<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'email', 'phone', 'location', 'latitude', 'longitude'];
    protected $dates = ['deleted_at', 'start_date'];

    public function agents()
    {
        return $this->hasMany(Agent::class);
    }
}
