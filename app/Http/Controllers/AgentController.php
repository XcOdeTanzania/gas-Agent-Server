<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Supplier;
use Illuminate\Http\Request;

class AgentController extends Controller
{

    public function postAgent(Request $request, $id)
    {
        $supplier = Supplier::find($id);

        if (!$supplier) {
            return response()->json(['message' => 'Document not found'], 404);
        }
        $agent = new Agent();

        $agent->name = $request->input('name');
        $agent->email = $request->input('email');
        $agent->phone = $request->input('phone');
        $agent->location = $request->input('location');
        $agent->latitude = $request->input('latitude');
        $agent->longitude = $request->input('longitude');

        $supplier->agents()->save($agent);


        return response()->json(['agent' => $agent], 201);
    }


    public function getAgents()
    {
        $agents = Agent::all();
        $response = [
            'agents' => $agents
        ];

        return response()->json($response, 200);
    }


    public function getAgent($agentId)
    {
        $agent = Agent::find($agentId);

        if (!$agent) {
            return response()->json(['message' => 'Document not found'], 404);
        }
        return response()->json([
            'venue' => $agent], 200);
    }

    public function getAllAgents($supplierId, $limit)
    {

        $supplier = Supplier::find($supplierId);
        if (!$supplier) {
            return response()->json(['message' => 'Document not found'], 404);
        }

        return response()->json([
            'agents' => $supplier->agents->take($limit)], 200);
    }


    public function putAgent(Request $request, $id)
    {
        $agent = Agent::find($id);
        if (!$agent) {
            return response()->json(['message' => 'Document not found'], 404);
        }
        $agent->update([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'location' => $request->input('location')
        ]);
        return response()->json(['agent' => $agent], 200);
    }


    public function deleteAgent($id)
    {
        $agent = Agent::find($id);
        if (!$agent) {
            return response()->json(['message' => 'Document not found'], 404);
        }

        $agent->delete();
        return response()->json(['message' => 'Agent has been deleted successfully'], 200);
    }
}
