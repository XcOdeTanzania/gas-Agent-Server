<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    protected $fillable = ['amount', 'status', 'order_id'];
    protected $dates = ['deleted_at', 'start_date'];

    public function orders(){
        return $this->hasOne(Order::class);
    }
}
